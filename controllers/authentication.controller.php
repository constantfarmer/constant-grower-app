<?php
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class authentication  
       * 
       */ 
       
      class __authentication {
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var message
           */ 
          public $message; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var
           */
          public $username;

          /**
           * @var
           */
          public $password;

          /**
           * @var
           */
          public $domain_prefix;

          /**
           * @var
           */
          public $DOlogin;

          /**
           * @var
           */
          public $sessionObj;

          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($debug){
              $this->dbObj                = new authenticationModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              $this->sessionObj           = new DB_Session(self::SessionConnect(), self::salty());
              $this->logobj               = new Logger();
              $this->debugging            = $debug;
              $this->dateset              = date('F j, Y, g:i a');
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
        echo "hello api";
          }


          public function __auth($params){
              $this->username       = $params["user_name"];
              $this->password       = md5($params["pass_word"]);
              $this->domain_prefix  = $params["cg_domain_prefix"];

              if (empty($this->username) || empty($this->password) || empty($this->domain_prefix)) {
                  self::setSessionVar("LOGIN_CHECK","EMPTY");
                  echo "EMPTY";
                  $this->logobj->logit("Empty login set attempted.");
              }else{
                  $this->DOlogin = $this->dbObj->db_auth_user("$this->username","$this->password","$this->domain_prefix");
                  if($this->DOlogin) {
                      self::setSessionVar("LOGIN_CHECK","OK");
                      self::setSessionVar("LOGGED_IN_USER",$this->username);
                      self::setSessionVar("DOMAIN_PREFIX",$this->domain_prefix);
                      echo "OK";
                      $this->logobj->logit("User  $this->username authenticated on domain $this->domain_prefix.constantgrower.com");
                      return (TRUE);
                  }else {
                      self::setSessionVar("LOGIN_CHECK","AUTH_FAIL");
                      echo "AUTH_FAIL";
                      $this->logobj->logit("User failed to log in with: $this->username:$this->password:$this->domain_prefix");
                      return (FALSE);
                  }
              }

          }
       
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->message("$msg"); 
          } 
       
       
      } 
 


