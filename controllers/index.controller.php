<?php
/**
 * The Wild West FrameWork
 * @copyright 2015
 *
 *
 * Class __index
 *
 */

class __index  extends SmartyView implements PageStruct {
    /**
     * use trait GeneralConfig
     */
    use GeneralConfig;
    /**
     * use trait Dbconfig
     */
    use DBConfig;
    /**
     * @var string
     */
    public $viewpath = '';
    /**
     * @var
     */
    public $smarty;

    /**
     * @var
     */
    private $dbObj;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var Logger
     */
    private $logobj;

    /**
     * @var
     */
    public $sessionObj;

    /**
     * @var string
     */
    public $login_check = "";

    /**
     * @var string
     */
    public $loggedin_user = "";

    /**
     * @var string
     */
    public $domain_prefix = "";

    /**
     * @var
     */
    public $DetectObj;

    /**
     * @param string $viewp
     * @param null $cache
     * @param null $debug
     */
public function __construct($viewp,$cache,$debug){

    $this->DetectObj         = new Mobile_detect();
    $this->logobj       	    = new Logger();
    if($this->DetectObj->isMobile()){
        $this->viewpath     = "$viewp/mobile";
        $this->cache        = "$cache/mobile";
        $this->logobj->logit("mobile view path loaded: ".$this->viewpath);
    }else {
        $this->viewpath     = "$viewp";
        $this->cache        = "$cache";
        $this->logobj->logit("standard view path loaded: ".$this->viewpath);
    }
    parent::__construct($this->viewpath , $this->cache , $debug);

    $this->dbObj                = new IndexModel(self::thedsn("mysql"),self::theuser(),self::thepass());
    $this->sessionObj           = new DB_Session(self::SessionConnect(), self::salty());


    $this->debugging            = "$debug";
    $this->dateset              = date('F j, Y, g:i a');
    $this->assign("dateset",$this->dateset);
    $this->login_check          = self::getSessionVar("LOGIN_CHECK");
    $this->loggedin_user        = self::getSessionVar("LOGGED_IN_USER");
    $this->domain_prefix        = self::getSessionVar("DOMAIN_PREFIX");
}

    /**
     * @return page default
     */
public function __default(){

    if ($this->login_check != "OK"){
        header("location: /login/");
    }else{
        //set some smarty globals for header
        $this->assign("user_loggedin",$this->loggedin_user);
        $this->assign("domain_pref", $this->domain_prefix);
        $this->global_header();
        $this->display('dashboard.tpl');
        $this->global_footer();
    }

}

    /**
     * @return page default
     */
    public function __dashboard(){

        if ($this->login_check != "OK"){
            header("location: /login/");
        }else{
            //set some smarty globals for header
            $this->assign("user_loggedin",$this->loggedin_user);
            $this->assign("domain_pref", $this->domain_prefix);
            if($this->DetectObj->isMobile()) {
                $this->mobile_header();
                $this->display('dashboard.tpl');
                $this->mobile_footer();
            }else{
                $this->global_header();
                $this->display('dashboard.tpl');
                $this->global_footer();
            }
        }

    }

    /**
     * logout
     */
    public function __logout(){
        if ($this->login_check != "OK"){
            header("location: /login/");
        }else{
            self::destroySession();
            header("location: /login/");

        }
    }

    /**
     * reset password
     * @param $params
     */
    public function __reset_password($params){
        $email      = $params["email"];
        $this->dbObj->lookup_password($email);
    }

    public function __register_new_user($params){
        $user        = $params["new_user_name"];
        $pass        = $params["new_pass_word"];
        $domain_pref = $params["new_cg_domain_prefix"];
        $email       = $params["new_email_address"];
        $this->logobj->logit("REGISTERING new user $user, $pass, $domain_pref, $email");


        if(!empty($user) && !empty($pass) && !empty($domain_pref) && !empty($email)) {
            $this->logobj->logit("REGISTERING new user $user, $pass, $domain_pref, $email");
            $reguser = $this->dbObj->register_new_user($user, $pass, $domain_pref, $email);
            if($reguser) {
                echo "SUCCESS";
            }else{
            echo "FAILED";
        }

        }else{
            $this->logobj->logit("REGISTERING new user data is empty");
            echo "EMPTY";
        }





    }


    /**
     * @return page phpinfo();
     */
    public function __Pinfo(){
        return(phpinfo());
    }

    /**
     * @return error page
     * @param $code
     */
    public function __error($code,$msg){
        $this->assign("error_code","$code");
        $this->assign("msg","$msg");
        $this->display("errors/$code.tpl");
    }



}


?>