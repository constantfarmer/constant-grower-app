<?php
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class login  
       * 
       */ 
       
      class __login  extends SmartyView {
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig;
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var
           */
          public $DetectObj;

          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){

              $this->DetectObj         = new Mobile_detect();
              $this->logobj       	    = new Logger();
              if($this->DetectObj->isMobile()){
                  $this->viewpath     = "$viewp/mobile";
                  $this->cache        = "$cache/mobile";
                  $this->logobj->logit("mobile view path loaded: ".$this->viewpath);
              }else {
                  $this->viewpath     = "$viewp";
                  $this->cache        = "$cache";
                  $this->logobj->logit("standard view path loaded: ".$this->viewpath);
              }
              parent::__construct($this->viewpath , $this->cache , $debug);

              $this->dbObj                = new loginModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              $this->sessionObj           = new DB_Session(self::SessionConnect(), self::salty());
              $this->logobj               = new Logger();
              $this->debugging            = $debug; 
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset);
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){
              if ($this->login_check != "OK") {
                  if($this->DetectObj->isMobile()){
                      $this->display('login.tpl');
                  }else{
                      $this->display('login.tpl');
                      $this->logobj->logit("login ui loaded");
                  }


                }else{
                  header("location: /?page=dashboard");
              }
          } 
       
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


