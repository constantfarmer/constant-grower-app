<?php  
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class seedfinder  
       * 
       */ 
       
      class __seedfinder { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var message
           */ 
          public $message; 
       
          /** 
           * @var 
           */ 
          public $login_check; 
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($debug){ 
              $this->dbObj                = new seedfinderModel(self::thedsn("mysql"),self::theuser(),self::thepass()); 
              $this->logobj               = new Logger(); 
              $this->debugging            = $debug; 
              $this->dateset              = date('F j, Y, g:i a'); 
              
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
        echo "hello api";
          }   
       
       
         public function __show_all(){
             if($this->debugging) {
                 $strains = $this->dbObj->get_all();
                 print($strains);
             }
             $breeders = $this->dbObj->breeder_export();
             $s = 0;
             $b = 0;
             //print_r($breeders);
             echo 'sync all breeders to db, click <a href="?page=sync_breeders">here</a><br>';
             echo 'sync all strainss to db, click <a href="?page=sync_strains">here</a><br>';
             foreach($breeders as $breeder){
                 $b++;
                 $br = str_replace(" ", "_",$breeder["name"]);
                 $logourl = 'http://en.seedfinder.eu/pics/00breeder/'.$breeder["logo"].'';
                 echo 'Breeder #'.$b.': '.$br.'|'.$breeder["name"].'<br>';
                 echo 'Logo: '.$logourl.' <br><img src="'.$logourl.'" border="0"><br>';
                 foreach($breeder["strains"] as $strain){
                     $s++;
                     $str = str_replace(" ", "_",$strain);
                     echo "Strain #$s: $str|$strain<br> ";
                 }
                 echo "<br>";
             }

         }

          public function __sync_breeders(){
              $breeders = $this->dbObj->breeder_export();
              if($this->dbObj->truncate_breeders()) {
                  foreach ($breeders as $breeder) {

                      $br_dispnam = str_replace(" ", "_", $breeder["name"]);
                      $logourl = 'http://en.seedfinder.eu/pics/00breeder/' . $breeder["logo"] . '';

                      $this->dbObj->import_breeder($breeder["name"], $br_dispnam, $logourl, $this->dateset);

                  }
              }else{
                  echo "Table locked, unable to truncate table cg_breeders<br>";
              }
          }

          public function __sync_strains(){
              $breeders = $this->dbObj->breeder_export();
              foreach($breeders as $breeder){
                 $brid = $this->dbObj->get_breeder_info_by_name($breeder["name"]);

                 $s    = 0;
                  foreach($breeder["strains"] as $strain){
                      $s++;
                      $str = str_replace(" ", "_",$strain);

                      echo 'Strain #'.$s.': '.$str.'|'.$strain.'  breeder id: '.$brid["breeder_id"].'<br> ';
                      echo $this->dbObj->import_strains($brid["breeder_id"],$str,$strain);
                  }



              }


          }
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->message("$msg"); 
          } 
       
       
      } 
 


