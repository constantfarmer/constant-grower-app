<?php  
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class configuration  
       * 
       */ 
       
      class __configuration  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var string
           */
          public $loggedin_user = "";

          /**
           * @var string
           */
          public $domain_prefix = "";

          /**
           * @var
           */
          public $DetectObj;
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new configurationModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              $this->sessionObj           = new DB_Session(self::SessionConnect(), self::salty());
              $this->DetectObj            = new Mobile_detect();
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp; 
              $this->cache                = $cache; 
              $this->debugging            = $debug; 
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset); 
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->loggedin_user        = self::getSessionVar("LOGGED_IN_USER");
              $this->domain_prefix        = self::getSessionVar("DOMAIN_PREFIX");
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{ 
                  $this->assign("view_path", "/configuration");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header(); 
                  $this->display('configuration.tpl'); 
                  $this->global_footer(); 
              } 
          }

          /**
           * cloningreports page
           */
          public function __system_management(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/configuration");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->assign("system_Settings",$this->dbObj->get_system_settings());
                  $this->assign("users",$this->dbObj->get_all_users($this->domain_prefix));
                  $this->global_header();
                  $this->display('configuration.tpl');

                  $this->global_footer();
              }
          }

          /**
           * cloningreports page
           */
          public function __backups(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/configuration");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header();
                  echo "backups";
                  //$this->display('backups.tpl');
                  $this->global_footer();
              }
          }

          public function __edit_system($params){
              $sysid        = $params["cfg_system_id"];
              $garden_name  = $params["cfg_garden_name"];
              $city         = $params["cfg_city"];
              $state        = $params["cfg_state"];
              $zip          = $params["cfg_zip"];

                  if($this->dbObj->edit_system_settings("$sysid","$garden_name","$state","$city","$zip")) {
                      echo "SUCCESS";
                  }else{
                      echo "FAILED";
                  }

          }

          public function __edit_user($params){
              $uID          = $params["USERID"];
              $user_name    = $params["edit_user_name"];
              $pass_word    = md5($params["edit_password"]);
              $alias_name   = $params["edit_alias_name"];
              $email        = $params["edit_email"];
              if($this->dbObj->edit_the_user($uID,$user_name,$pass_word,$alias_name,$email)) {
                  echo "SUCCESS";
              }else{
                  echo "FAILED";
              };
          }



          public function __delete_user($params){
              $uID   = $params["ID"];
              echo "FAILED";
          }
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


