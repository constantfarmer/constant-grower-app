<?php  
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class cloning  
       * 
       */ 
       
      class __cloning  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var string
           */
          public $loggedin_user = "";

          /**
           * @var string
           */
          public $domain_prefix = "";

          /**
           * @var
           */
          public $DetectObj;


          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new cloningModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              $this->sessionObj           = new DB_Session(self::SessionConnect(), self::salty());
              $this->DetectObj            = new Mobile_detect();
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp; 
              $this->cache                = $cache; 
              $this->debugging            = $debug; 
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset); 
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->loggedin_user        = self::getSessionVar("LOGGED_IN_USER");
              $this->domain_prefix        = self::getSessionVar("DOMAIN_PREFIX");

          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{ 
                  $this->assign("view_path", "/cloning");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header(); 
                  $this->display('cloning.tpl'); 
                  $this->global_footer(); 
              } 
          }

          /**
           * return page default
           */
          public function __activelist(){
              self::__default();
          }

          /**
           * cloningreports page
           */
          public function __reports(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/cloning");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header();
                  echo "Cloning Reports";
                  //$this->display('cloning_reports.tpl');
                  $this->global_footer();
              }
          }
       
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


