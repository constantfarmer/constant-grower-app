<?php  
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class leafly  
       * 
       */ 
       
      class __leafly {
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
       
          /** 
           * @var 
           */ 
          public $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var message
           */ 
          public $message; 
       
          /** 
           * @var 
           */ 
          public $login_check; 
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($debug){

              $this->dbObj                = new leaflyModel(self::thedsn("mysql"),self::theuser(),self::thepass()); 
              $this->logobj               = new Logger();
              $this->debugging            = $debug; 
              $this->dateset              = date('F j, Y, g:i a');
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
        echo "hello api";
        print_r($this->dbObj->get_all_strains());
          }
          
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->message("$msg"); 
          } 
       
       
      } 
 


