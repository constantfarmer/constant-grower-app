<?php  
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class about  
       * 
       */ 
       
      class __about  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var string
           */
          public $loggedin_user = "";

          /**
           * @var string
           */
          public $domain_prefix = "";

          /**
           * @var
           */
          public $DetectObj;
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new aboutModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              $this->sessionObj           = new DB_Session(self::SessionConnect(), self::salty());
              $this->DetectObj            = new Mobile_detect();
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp; 
              $this->cache                = $cache; 
              $this->debugging            = $debug; 
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset); 
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->loggedin_user        = self::getSessionVar("LOGGED_IN_USER");
              $this->domain_prefix        = self::getSessionVar("DOMAIN_PREFIX");

          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{ 
                  $this->assign("view_path", "/about");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header(); 
                  $this->display('about.tpl'); 
                  $this->global_footer(); 
              } 
          }


          /**
           * @return page support
           */
          public function __support(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/about");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header();
                  echo "Support Page";
                  //$this->display('support.tpl');
                  $this->global_footer();
              }
          }


          /**
           * @return page contact
           */
          public function __contact(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/about");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header();
                  echo "Contact Us";
                  //$this->display('contact.tpl');
                  $this->global_footer();
              }
          }

          /**
           * @return page default
           */
          public function __release_info(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/about");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header();
                  echo "Release INfo";
                  //$this->display('release_info.tpl');
                  $this->global_footer();
              }
          }

          /**
           * @return page default
           */
          public function __tos(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/about");
                  $this->assign("user_loggedin",$this->loggedin_user);
                  $this->assign("domain_pref", $this->domain_prefix);
                  $this->global_header();
                  echo "Terms Of Service";
                  //$this->display('tos.tpl');
                  $this->global_footer();
              }
          }

          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


