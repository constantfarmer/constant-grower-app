<?php
      /**     
       * The Wild West FrameWork  
       * @copyright 2015  
       *  
       * Class yahoo_weather
       * Client ID (Consumer Key)
      dj0yJmk9cDUxQ1FYRDRVTXdDJmQ9WVdrOWJIQk1hV3RWTjJrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yOA--
      Client Secret (Consumer Secret)
      31851a5e0679e567a99333f17731e29b1ca2a907
       * 
       */ 
       
      class __yahoo_weather{
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig;

          /**
           * use trait YahooWeatherAPI
           */
          use YahooWeatherAPI;
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var message
           */ 
          public $message; 
       
          /** 
           * @var 
           */ 
          public $login_check; 
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($debug){
              $this->dbObj                = new yahoo_weatherModel(self::thedsn("mysql"),self::theuser(),self::thepass()); 
              $this->logobj               = new Logger();
              $this->debugging            = $debug; 
              $this->dateset              = date('F j, Y, g:i a'); 

          } 

          public function __show_Weather($params){
              $zip          = $params["zipcode"];
              $weatherObj   = $this->dbObj->get_weather($zip);
              print_r($weatherObj);
              //print(json_encode($weatherObj));
          }
       
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->message("$msg"); 
          } 
       
       
      } 
 


