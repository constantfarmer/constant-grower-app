<?php
/**
 * The Wild West FrameWork
 * @copyright 2015
 *
 * Class yahoo_weather
 * Client ID (Consumer Key)
dj0yJmk9cDUxQ1FYRDRVTXdDJmQ9WVdrOWJIQk1hV3RWTjJrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yOA--
Client Secret (Consumer Secret)
31851a5e0679e567a99333f17731e29b1ca2a907
 *
 */

require_once( dirname(__FILE__).'/../config/config.common.php');
Load::model("yahoo_weather");

class yahoo_weather_service{
    /**
     * use trait GeneralConfig
     */
    use GeneralConfig;
    /**
     * use trait DBConfig
     */
    use DBConfig;

    /**
     * use trait YahooWeatherAPI
     */
    use YahooWeatherAPI;

    /**
     * @var
     */
    private $dbObj;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var Logger
     */
    private $logobj;

    /**
     * @var message
     */
    public $message;

    /**
     * @param string $viewp
     * @param null $cache
     * @param null $debug
     */
    public function __construct($debug){
        $this->dbObj                = new yahoo_weatherModel(self::thedsn("mysql"),self::theuser(),self::thepass());
        $this->logobj               = new Logger();
        $this->debugging            = $debug;
        $this->dateset              = date('F j, Y, g:i a');

    }

    public function show_Weather($zip){
        $weatherObj   = $this->dbObj->get_weather($zip);
        print_r($weatherObj);
        //print(json_encode($weatherObj));
    }

    public function sync_Weather($zip){
        $weatherObj   = $this->dbObj->get_weather($zip);
        $wq = $weatherObj["query"];

        $wqcount  = $wq["count"];
        $wcreated = $wq["created"];
        $wresults  = $wq["results"]["channel"];

        $wtitle   = $wresults["title"];

        echo "title $wtitle\n";

       // print();
        //return($weatherObj);
    }


    /**
     * @return error page
     * @param $code
     */
    public function __error($code,$msg){
        $this->message("$msg");
    }


}



