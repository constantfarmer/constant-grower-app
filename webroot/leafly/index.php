<?php 
/** 
* The Wild West FrameWork 
* @copyright 2015 
* 
*/ 


require_once( dirname(__FILE__).'/../../config/config.common.php');
$viewpath = basename(__DIR__);
Load::model("leafly"); 
Load::controller("leafly"); 


/** 
* Entry view object 
*/ 
$ViewObj = new __leafly("views/$viewpath","webroot/$viewpath",$_REQUEST['cache'],$_REQUEST['debug']); 
          if(isset($_REQUEST['page'])){ 
           
              $pagereq  = "__" . $_REQUEST['page']; 
              $params   = $_REQUEST; 
           
              if (method_exists($ViewObj, "$pagereq")) { 
                  $thepage = $ViewObj->$pagereq($params); 
              }else{ 
          
                  echo "page object does not exist, exiting"; 
                  exit; 
              } 
          
          
          }else{
              $ViewObj->__default();
          
          }
          


