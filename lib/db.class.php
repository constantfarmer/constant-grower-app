<?php
/**
 * The Wild West FrameWork
 * @copyright 2015
 *
 * Class MasterDb
 */
class MasterDb extends PDO {
    /**
     * @var string
     */
    public $error;

    /**
     * @var
     */
   public $sql;

    /**
     * @var
     */
    public $bind;

    /**
     * @var
     */
    public $errorCallbackFunction;

    /**
     * @var
     */
    public $msg;

    /**
     * @param $dsn
     * @param string $user
     * @param string $passwd
     */
    public function __construct($dsn, $user = "", $passwd = "") {
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    /**
     * @method debug
     */
    public function debug() {
        $this->msg = "";

        if(!empty($this->errorCallbackFunction)) {

            $error = array("Error" => $this->error);

            if(!empty($this->sql)) {
                $error["SQL Statement"] = $this->sql;
            }

            if(!empty($this->bind)) {
                $error["Bind Parameters"] = trim(print_r($this->bind, true));
            }
            $backtrace = debug_backtrace();
            if(!empty($backtrace)) {
                foreach($backtrace as $info) {
                    if($info["file"] != __FILE__)
                        $error["Backtrace"] = $info["file"] . " at line " . $info["line"];
                }
            }



                if(!empty($error["Bind Parameters"])) {
                    $error["Bind Parameters"] = "" . $error["Bind Parameters"] . "";
                    $this->msg .= "\t\tSQL Error";
                    foreach ($error as $key => $val) {
                        $this->msg .= "\t" . $key . " - " . $val;
                        $this->msg .= "\n\t";
                    }
                }


            $func = $this->errorCallbackFunction;
            $func($this->msg);
        }
    }


    /**
     * @param $conn
     * @return null
     */
    public function close($conn){
        unset($conn);
        return(NULL);
    }


    /**
     * @return array
     */
    public function get_drivers(){
        $driver_avail = parent::getAvailableDrivers();
        return($driver_avail);
    }


    /**
     * @param $query
     * @return mixed
     */
    public function query_single($query){
        $q = parent::query("$query");
        $e  = $q->fetch(PDO::FETCH_ASSOC);
        return($e);
    }

    /**
     * @param $query
     * @return array
     */
    public function query_all($query){
        $q = parent::query($query);
        $e = $q->fetchAll(PDO::FETCH_BOTH);
        return($e);
    }

    /**
     * @param $query
     * @return array
     */
    public function query_obj($query){
        $q = parent::query($query);
        $e = $q->fetchAll(PDO::FETCH_OBJ);
        return ($e);
    }

    /**
     *@return string
     */
    public function pingDb(){
        return(self::query_single("SELECT 1"));
    }
    
        /**
     * @param $table
     * @param $field
     * @param bool|true $sorted
     * @return array
     */
    public function query_enumerated_values($table,$field,$sorted=true){

        $this->enum      = self::query_single('SHOW COLUMNS FROM '.$table.';');
        while( $this->enum ){
            if($this->enum['Field'] == $field){

                $this->types    = $this->enum['Type'];
                $begin_str      = strpos($this->types,"(")+1;
                $end_str        = strpos($this->types,")");
                $this->types    = substr($this->types, $begin_str, $end_str-$begin_str);
                $this->types    = str_replace("'","",$this->types);
                $this->types    = explode(',',$this->types);
                if($sorted){
                    sort($this->types);
                }
                break;

            }
        }

        return($this->types);
    }


    public function query_pagination($query, $per_page = 10,$page = 1, $url = '?'){
        $row        = self::query_assoc("$query");
        $total      = $row['total'];
        $adjacents  = "2";

        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;

        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total/$per_page);
        $lpm1 = $lastpage - 1;

        $pagination = "";
        if($lastpage > 1){
            $pagination .= "<ul id='stop' class='pagination'>";
            $pagination .= "<li class='details'>Page $page of $lastpage</li>";
            if ($lastpage < 7 + ($adjacents * 2)){
                for ($counter = 1; $counter <= $lastpage; $counter++){
                    if ($counter == $page){
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    }else{
                        $pagination.= "<li><a href='{$url}pageset=$counter'>$counter</a></li>";
                    }
                }
            }elseif($lastpage > 5 + ($adjacents * 2)){
                if($page < 1 + ($adjacents * 2)){
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
                        if ($counter == $page){
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        }else{
                            $pagination.= "<li><a href='{$url}pageset=$counter'>$counter</a></li>";
                        }
                    }
                    $pagination.= "<li class='dot'>...</li>";
                    $pagination.= "<li><a href='{$url}pageset=$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}pageset=$lastpage'>$lastpage</a></li>";
                }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
                    $pagination.= "<li><a href='{$url}pageset=1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}pageset=2'>2</a></li>";
                    $pagination.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
                        if ($counter == $page){
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        }else{
                            $pagination.= "<li><a href='{$url}pageset=$counter'>$counter</a></li>";
                        }
                    }
                    $pagination.= "<li class='dot'>..</li>";
                    $pagination.= "<li><a href='{$url}pageset=$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}pageset=$lastpage'>$lastpage</a></li>";
                }else{
                    $pagination.= "<li><a href='{$url}pageset=1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}pageset=2'>2</a></li>";
                    $pagination.= "<li class='dot'>..</li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
                        if ($counter == $page){
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        }else{
                            $pagination.= "<li><a href='{$url}pageset=$counter'>$counter</a></li>";
                        }
                    }
                }
            }

            if ($page < $counter - 1){
                $pagination.= "<li><a href='{$url}pageset=$next'>Next</a></li>";
                $pagination.= "<li><a href='{$url}pageset=$lastpage'>Last</a></li>";
            }else{
                $pagination.= "<li><a class='current'>Next</a></li>";
                $pagination.= "<li><a class='current'>Last</a></li>";
            }
            $pagination.= "</ul><br>";
        }


        return ($pagination);



    }


}


/**
 * Class legacyDb old school
 * @deprecated
 */
class legacyDb {
    /**
     * @var string
     */
    private $host      = DB_HOST;
    /**
     * @var string
     */
    private $user      = DB_LOGIN;
    /**
     * @var string
     */
    private $pass      = DB_PASS;
    /**
     * @var string
     */
    private $dbname    = DB_DB;
    /**
     * @var
     */
    private $connection;
    /**
     * @var
     */
    private $error;

    public $sql = "";

    public function __construct(){

        //default utf8 character set
        mysql_set_charset('utf8', self::legacy_connect());

    }


    /**
     * @return resource
     */
    protected function legacy_connect() {
        $this->connection = mysql_connect($this->host, $this->user, $this->pass)
        or die ("\nCould not connect to MySQL server\n");
        mysql_select_db($this->dbname,$this->connection)
        or die ("\nCould not select the indicated database\n");
        return ($this->connection);
    }

    /**
     * @param $sql
     * @return resource|string
     */
    public function legacy_query($sql){
        $this->sql = mysql_query(mysql_real_escape_string($sql,self::legacy_connect()));
        return ($this->sql);
    }


}




