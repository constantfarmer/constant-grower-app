#!/bin/bash

THEDOMAIN="dev.constantgrower.int"
COUNTRY="US"
STATE="Colorado"
CITY="Denver"
COMPANY="CGNet, LLC"
DEPARTMENT="Constant Farmer"

#DONT MONKEY WITH THE MONKEY
########################################
#ssl csr generator
THISDUR=`pwd`
DONE=`date +%Y-%m-%d`
MAILADDR="admin@constantgrower.com"
red='\e[0;31m'
RED='\e[1;31m'
blue='\e[0;34m'
BLUE='\e[1;34m'
cyan='\e[0;36m'
CYAN='\e[1;36m'
NC='\e[0m'
#######################################

echo -e "##############################################################################################"
echo -e "#       ${CYAN}THE COMPANY NETWORKS                                                      ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "${NC}# ${BLUE} Patch       ${RED}:${BLUE} null                                           ${NC}"
echo -e "${NC}# ${BLUE} Server      ${RED}:${BLUE} $HOSTNAME                                      ${NC}"
echo -e "${NC}# ${BLUE} Date        ${RED}:${BLUE} $DONE                                          ${NC}"
echo -e "${NC}# ${BLUE} Kernel      ${RED}:${BLUE} `uname -r`                                     ${NC}"
echo -e "${NC}# ${BLUE} Architecture${RED}:${BLUE} `uname -m`                                     ${NC}"
echo -e "${NC}# ${BLUE} Processor   ${RED}:${BLUE} `uname -p`                                     ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "${NC}# ${BLUE}Support                                                                    ${NC}"
echo -e "${NC}# ${BLUE}  Email     ${RED}: ${BLUE}$MAILADDR                                        ${NC}"
echo -e "${NC}# ${BLUE}  Server URL${RED}: ${BLUE}http://`uname -n`                               ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "${NC}#########################################################################################"
sleep 2

if [ -d "./ssl/$THEDOMAIN" ] ; then
    echo -e -n "domain exists $THEDOMAIN, now entering directory....\n"
    cd "./ssl/$THEDOMAIN"
    openssl req -nodes -newkey rsa:2048 -nodes -keyout "$THEDOMAIN.key" -out "$THEDOMAIN.csr" -subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$COMPANY/OU=$DEPARTMENT/CN=$THEDOMAIN"
    openssl x509 -req -days 5475 -in "$THEDOMAIN.csr" -signkey "$THEDOMAIN.key" -out "$THEDOMAIN.crt"
else
    mkdir -pv "./ssl/$THEDOMAIN"
    cd "./ssl/$THEDOMAIN"
    echo -e -n "made new domain dir $THEDOMAIN\n"
    openssl req -nodes -newkey rsa:2048 -nodes -keyout "$THEDOMAIN.key" -out "$THEDOMAIN.csr" -subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$COMPANY/OU=$DEPARTMENT/CN=$THEDOMAIN"
    openssl x509 -req -days 5475 -in "$THEDOMAIN.csr" -signkey "$THEDOMAIN.key" -out "$THEDOMAIN.crt"
fi

#Verify/Check certificate
#Verify a certificate and key matches
openssl x509 -noout -modulus -in "$THEDOMAIN.crt" | openssl md5
openssl rsa -noout -modulus -in "$THEDOMAIN.key" | openssl md5

#Check certificate
openssl x509 -in "$THEDOMAIN.crt" -text -noout

#Check key
openssl rsa -in "$THEDOMAIN.key" -check

#Check a CSR
openssl req -text -noout -verify -in "$THEDOMAIN.csr"