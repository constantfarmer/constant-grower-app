#!/bin/bash

#setup nginx
sudo mkdir -pv /etc/nginx/{sites-enabled,sites-available} && \
sudo ln -vs /var/www/dev.constantgrower.int/setup/nginx/sites-available/dev.constantgrower.int.conf /etc/nginx/sites-available/dev.constantgrower.int.conf && \
sudo ln -vs /etc/nginx/sites-available/dev.constantgrower.int.conf /etc/nginx/sites-enabled/dev.constantgrower.int.conf && \
sudo cp -rfv /var/www/dev.constantgrower.int/setup/nginx/ssl /etc/nginx/ssl && \
sudo cp -rfv /var/www/dev.constantgrower.int/setup/nginx/nginx.conf /etc/nginx/nginx.conf;



#setup php,  redis, memcache
#redis
sudo cp -fv  /var/www/dev.constantgrower.int/setup/redis/redis.conf /etc/redis.conf;
sudo chkconfig redis on;
sudo service redis start;
sudo redis-cli -a CGRedis -r 1 -i 1 info;

#memcache
sudo chkconfig memcached on;
sudo service memcached start;
sudo memcached-tool 127.0.0.1:11211 stats;

#get ssh mode, compile and install
sudo yum -y install libssh2-devel && \
cd /tmp && sudo  wget https://github.com/Sean-Der/pecl-networking-ssh2/archive/master.zip && sudo unzip master.zip; sudo chown -vR vagrant.vagrant pecl* && \
cd pecl-networking-ssh2-master/ && \
sudo phpize && sudo  ./configure && sudo  make && sudo make install;
#confgire php-fpm
cd /var/www/dev.constantgrower.int/setup/;
sudo cp -rfv ./php/php.d/10-ssh2.ini /etc/php.d/10-ssh2.ini; php -m | grep ssh2;
sudo cp -rfv ./php/php-fpm.d/www.conf /etc/php-fpm.d/www.conf;
sudo cp -rfv ./php/php.ini /etc/php.ini;
sudo cp -rfv ./php/php-fpm.conf /etc/php-fpm.conf;

#download and compile memcache
cd /tmp; sudo rm -rvf *.zip && sudo rm -rf pecl* && sudo rm -rf php* && \
sudo git clone https://github.com/php-memcached-dev/php-memcached;
cd /tmp/php-memcached/ && sudo git checkout php7 && sudo phpize && sudo ./configure --disable-memcached-sasl && sudo make && sudo make install && \
sudo echo -e -n ";Enable Memcached Extension\nextension=memcached.so\n" | tee --append /etc/php.d/20-memcached.ini && sudo php -m | grep memcached

cd /var/www/dev.constantgrower.int/setup/;



#configure services
sudo chkconfig mysql on;
sudo chkconfig php-fpm on;
sudo chkconfig nginx on;

#start services
sudo service mysql start;
sudo service php-fpm start;
sudo service nginx start;


#misc
sudo chmod -R g+rwx /var/www/; sudo chmod -vR g+wrx /var/lib/php/; sudo chmod -vR 7777 /var/lib/php/;
sudo gpasswd -a nginx vagrant; sudo gpasswd -a vagrant nginx;
sudo gpasswd -a nginx php-fpm; sudo gpasswd -a php-fpm nginx;
sudo gpasswd -a vagrant php-fpm; sudo gpasswd -a php-fpm vagrant;

#setup db
sudo mysql --login-path="cg_localhost" -v -e "CREATE DATABASE constant_grower";
sudo mysql --login-path="cg_localhost" -v -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED by ''; FLUSH PRIVILEGES;";
sudo mysql --login-path="cg_localhost" -v constant_grower < /var/www/dev.constantgrower.int/setup/sql/dev.constantgrower.int.schema.sql && \
sudo mysql --login-path="cg_localhost" constant_grower < /var/www/dev.constantgrower.int/setup/sql/dev.constantgrower.int.devdata.sql

#install deps
sudo -u vagrant -H bash -c "cd /vagrant; rm -rf vendor/ &&  /vagrant/bin/composer install && /vagrant/bin/composer update;"

#restart services
sudo service php-fpm restart;
sudo service nginx restart;
sudo service mysql restart;

#SEtup nvm
sudo runuser - vagrant -c "nvm install 8 && npm install -g pm2@latest gulp@latest newman@latest;"
#Checm mysql
 sudo mysql --login-path="cg_localhost" -v -e "SHOW GLOBAL VARIABLES WHERE Variable_name LIKE '%version_comment%';" | grep version_comment;
 echo -e -n "\n\n"
#Check web
curl --silent --cacert /etc/nginx/ssl/dev.constantgrower.int/dev.constantgrower.int.crt "https://dev.constantgrower.int/api/?page=setup_complete" && echo -e -n "\n\n"