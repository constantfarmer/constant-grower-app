-- MySQL dump 10.13  Distrib 5.6.37-82.2, for Linux (x86_64)
--
-- Host: localhost    Database: constant_grower
-- ------------------------------------------------------
-- Server version	5.6.37-82.2-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50112 SELECT COUNT(*) INTO @is_rocksdb_supported FROM INFORMATION_SCHEMA.SESSION_VARIABLES WHERE VARIABLE_NAME='rocksdb_bulk_load' */;
/*!50112 SET @save_old_rocksdb_bulk_load = IF (@is_rocksdb_supported, 'SET @old_rocksdb_bulk_load = @@rocksdb_bulk_load', 'SET @dummy_old_rocksdb_bulk_load = 0') */;
/*!50112 PREPARE s FROM @save_old_rocksdb_bulk_load */;
/*!50112 EXECUTE s */;
/*!50112 SET @enable_bulk_load = IF (@is_rocksdb_supported, 'SET SESSION rocksdb_bulk_load = 1', 'SET @dummy_rocksdb_bulk_load = 0') */;
/*!50112 PREPARE s FROM @enable_bulk_load */;
/*!50112 EXECUTE s */;
/*!50112 DEALLOCATE PREPARE s */;

--
-- Table structure for table `cg_assets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_assets` (
  `ID` int(11) NOT NULL,
  `grow_room_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `type` enum('Cash','Inventory','Receivables','Hardware','Intangible','Tangible') NOT NULL,
  `tags` text NOT NULL,
  `date_added` date NOT NULL,
  `owner` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_breeders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_breeders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `logo_url` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_can_strains`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_can_strains` (
  `ID` int(11) NOT NULL,
  `tagID` varchar(55) NOT NULL,
  `key` varchar(155) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(155) NOT NULL,
  `symbol` varchar(155) NOT NULL,
  `abstract_desc` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `detail_url` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL,
  `top_effect` varchar(155) NOT NULL,
  `top_medical` varchar(155) NOT NULL,
  `top_activity` varchar(155) NOT NULL,
  `top_negative` varchar(155) NOT NULL,
  PRIMARY KEY (`ID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_cloning`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_cloning` (
  `ID` int(11) NOT NULL,
  `clone_tagID` varchar(255) NOT NULL,
  `parent_plant_ID` varchar(55) NOT NULL,
  PRIMARY KEY (`ID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_images` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` longblob NOT NULL,
  `type` varchar(255) NOT NULL,
  `size` varchar(50) NOT NULL,
  `grow_room_id` varchar(25) NOT NULL,
  `plant_id` varchar(25) NOT NULL,
  `asset_id` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_patients`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_patients` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_plants`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_plants` (
  `ID` int(11) NOT NULL,
  `plant_tagID` varchar(255) NOT NULL,
  `grow_room_id` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL DEFAULT '0',
  `height` varchar(255) NOT NULL,
  `location` enum('Indoor','Outdoor') NOT NULL,
  `generation` varchar(55) NOT NULL,
  `status` enum('Germinating','Vegging','Flowering','Harvested','Dead') NOT NULL DEFAULT 'Germinating',
  `image` longblob NOT NULL,
  `notes` text NOT NULL,
  `dna_inheritance` enum('Seed','Mother','Father','Clone') NOT NULL,
  `dna_gender` enum('Male','Female','Hermaphrodite','Unkown') NOT NULL,
  `nute_ph` varchar(100) NOT NULL,
  `nute_ppm` varchar(100) NOT NULL,
  `nute_temp` varchar(100) NOT NULL,
  `nute_date` varchar(255) NOT NULL,
  `plant_strain` varchar(255) NOT NULL,
  `date_added` varchar(255) NOT NULL,
  `date_planted` varchar(255) NOT NULL,
  `date_flushed` varchar(255) NOT NULL,
  `date_cloned` varchar(255) NOT NULL,
  `date_flowered` varchar(255) NOT NULL,
  `date_harvested` varchar(255) NOT NULL,
  `date_packaged` varchar(255) NOT NULL,
  `days_in_veg` varchar(155) NOT NULL,
  `days_in_flower` varchar(155) NOT NULL,
  `days_in_cure` varchar(155) NOT NULL,
  PRIMARY KEY (`ID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_rooms`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_rooms` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `picture` longblob NOT NULL,
  `type` enum('Hydroponic','Soil','Aeroponic','Mixed') NOT NULL,
  `humidity` int(10) NOT NULL,
  `temp` int(10) NOT NULL,
  `date_built` varchar(255) NOT NULL,
  `tags` text NOT NULL,
  `power_type` enum('110 volt','220 volt','240 volt') NOT NULL,
  `power_amps` enum('15 amp','20 amp','30 amp') NOT NULL,
  `power_sockets` int(10) NOT NULL,
  `power_usage` int(10) NOT NULL,
  `date_added` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_sessions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_sessions` (
  `id` char(32) NOT NULL,
  `data` longtext NOT NULL,
  `last_accessed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_strain`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_strain` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `breeder_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_system_settings`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_system_settings` (
  `id` int(11) NOT NULL,
  `garden_name` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_code` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_users` (
  `ID` int(11) NOT NULL,
  `CGUser` varchar(255) NOT NULL,
  `CGPass` varchar(255) NOT NULL,
  `CGLevel` int(10) NOT NULL,
  `CGDomain_prefix` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `alias_name` varchar(255) NOT NULL,
  `signup_date` varchar(255) NOT NULL,
  `last_login` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `email` (`email`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cg_weather`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_weather` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `lastBuildDate` varchar(255) NOT NULL,
  `ttl` int(10) NOT NULL,
  `location_city` varchar(255) NOT NULL,
  `location_country` varchar(255) NOT NULL,
  `location_region` varchar(255) NOT NULL,
  `units_distance` varchar(55) NOT NULL,
  `units_pressure` varchar(55) NOT NULL,
  `units_speed` varchar(55) NOT NULL,
  `units_temprature` varchar(55) NOT NULL,
  `wind_chill` int(10) NOT NULL,
  `wind_direction` int(10) NOT NULL,
  `wind_speed` int(10) NOT NULL,
  `atmosphere_humidity` int(10) NOT NULL,
  `atmosphere_pressure` int(10) NOT NULL,
  `atmosphere_rising` int(10) NOT NULL,
  `atmosphere_visibility` int(10) NOT NULL,
  `astronomy_sunrise` varchar(55) NOT NULL,
  `astronomy_sunset` varchar(55) NOT NULL,
  `image_title` varchar(255) NOT NULL,
  `image_width` varchar(55) NOT NULL,
  `image_height` varchar(55) NOT NULL,
  `image_link` text NOT NULL,
  `image_url` text NOT NULL,
  `item_title` varchar(255) NOT NULL,
  `item_lat` varchar(25) NOT NULL,
  `item_long` varchar(25) NOT NULL,
  `item_link` varchar(255) NOT NULL,
  `item_pubDate` varchar(255) NOT NULL,
  `item_cond_code` int(10) NOT NULL,
  `item_cond_date` varchar(255) NOT NULL,
  `item_cond_temp` int(10) NOT NULL,
  `item_cond_txt` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `item_forecast_0_code` int(10) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `session_data`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_data` (
  `session_id` varchar(32) NOT NULL DEFAULT '',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `session_data` blob NOT NULL,
  `session_expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50112 SET @disable_bulk_load = IF (@is_rocksdb_supported, 'SET SESSION rocksdb_bulk_load = @old_rocksdb_bulk_load', 'SET @dummy_rocksdb_bulk_load = 0') */;
/*!50112 PREPARE s FROM @disable_bulk_load */;
/*!50112 EXECUTE s */;
/*!50112 DEALLOCATE PREPARE s */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-17 17:42:05
