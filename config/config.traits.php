<?php
/**
 * The Wild West FrameWork
 * @copyright 2015
 *
 */

 /**
 * Define environment, are we local vagrant, development or production?
 */


 /**
 * Trait DBConfig
 */
trait DBConfig{

    /**
     * @return string
     */
    public function get_dbenv(){
        return(getenv("APP_ENV_TYPE"));
    }
    /**
     * @return string
     */
    public function thehost(){
        return "localhost";
    }
 
    /**
     * @return string
     */
    public function theuser(){
        return "root";
    }

    /**
     * @return string
     */
    public function thepass(){
        return "";
    }

    /**
     * @return string
     */
    public function thedbname(){
        return "constant_grower";
    }

    /**
     * @return int
     */
    public function thedbport(){
        return(3306);
    }
    /**
     * @return string
     */
    public  function salty(){
        return('x09Cod$_3CR&iT');
    }

    /**
     * @param $drivertype
     * @return int|string
     */
    public function thedsn($drivertype){
        switch($drivertype){
            case "mysql":
                $dsn = "mysql:host=".self::thehost().";dbname=".self::thedbname().";port=3306";
                return ($dsn);
                break;
            case "pgsql":
                $dsn = "pgsql:host=".self::thehost().";dbname=".self::thedbname().";port=5432";
                return ($dsn);
                break;
            case "mssql":
                $dsn = "dblib:host=".self::thehost().";dbname=".self::thedbname().";port=1433";
                return ($dsn);
                break;
            case "mongodb":
                $dsn = "NOT";
                return($dsn);
            default:
                return 3306;
        }
    }

    public function SessionConnect(){
        return(mysqli_connect(self::thehost(),self::theuser(),self::thepass(),self::thedbname(),self::thedbport()));
    }


}

/**
 * Class SeedFinderAPI
 */
trait SeedFinderAPI{

    /**
     * @return string
     */
    public function sf_api_key(){
        return("9df80cff63b1644546c899124e33d860");
    }
}

/**
 * Class LeaflyAPI
 */
trait LeaflyAPI{
    /**
     * @return null
     */
    public function leaf_api_key(){
        return(NULL);
    }

    /**
     * @return null
     */
    public function leaf_api_id(){
        return(NULL);
    }
}
/**
 * Trait GeneralConfig
 */
trait GeneralConfig{
    
        /**
     * @return string
     */
    public function log_path(){
        return(dirname( __FILE__ ) . '/../logs/');
    }
    
    /**
     * @return string
     */
    public function get_env(){
        return(WILD_WEST_ENV);
    }

    /**
     * @return string
     */
    public function mycli(){
        return('/usr/bin/mysql');
    }

    public function redirectToHome(){
        return(header("Location: /"));
    }

    public function destroySession(){
        return(session_destroy());
    }

    public function startSession(){
        return(session_start());
    }

    public function setSessionVar($var,$value){
        return($_SESSION["$var"] = "$value");
    }

    public function getSessionVar($varname){
        return($_SESSION["$varname"]);
    }

    /**
     * @return string
     */
    public static function model_path(){
        return(__DIR__.'/../models');
    }

    /**
     * @return string
     */
    public static function controller_path(){
        return(__DIR__.'/../controllers');
    }

    /**
     * @return string
     */
    public static function lib_path(){
        return(__DIR__.'/../lib');
    }

    /**
     * @return string
     */
    public static function service_path(){
        return(__DIR__.'/../services');
    }

    /**
     * @return string
     */
    public static function vendor_path(){
        return(__DIR__.'/../vendor');
    }

    /**
     * @return string
     */
    public static function view_path(){
        return(__DIR__.'/../views');
    }

    /**
     * @return string
     */
    public static function webview_path(){
        return(__DIR__.'/../webroot');
    }

}

/**
 * Trait MemcacheConfig
 */
trait MemcacheConfig{

    /**
     * @return array
     */
    public function memcache_server_list(){
        $servers = array('localhost:11211','127.0.0.1:11211');
        return ($servers);
    }

    /**
     * @return string
     */
    public function memcache_prefix(){
        return(APP_NAME);
    }

    /**
     * @return string
     */
    public function field_prefix(){
    return(APP_NAME._FIELD_);
    }

    /**
     * @return string
     */
    public function table_prefix(){
        return(APP_NAME._TABLE_);
    }


}


trait YahooWeatherAPI{

    /**
     * Client ID (Consumer Key)
    dj0yJmk9cDUxQ1FYRDRVTXdDJmQ9WVdrOWJIQk1hV3RWTjJrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yOA--
    Client Secret (Consumer Secret)
    31851a5e0679e567a99333f17731e29b1ca2a907
     */

    /**
     * @return string
     */
    public function yahoo_base_url(){
        return("http://query.yahooapis.com/v1/public/yql");
    }

    /**
     * @return string
     */
    public function yahoo_client_id(){
        return("dj0yJmk9cDUxQ1FYRDRVTXdDJmQ9WVdrOWJIQk1hV3RWTjJrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yOA--");
    }

    /**
     * @return string
     */
    public function yahoo_client_secret(){
        return("31851a5e0679e567a99333f17731e29b1ca2a907");
    }



}


trait OpenWeatherMap{

    /**
     * @return string
     */
    public function owm_api_key(){
        return("b55a6566aaabef877181524bf4d00461");
    }

}
