<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Constant Grower</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/jquery-ui.css" rel="stylesheet">

<meta name="seedfinderverification" content="Open Sesame!" />
    <style>
        @-webkit-viewport { width: device-width; }
        @-moz-viewport { width: device-width; }
        @-ms-viewport { width: device-width; }
        @-o-viewport { width: device-width; }
        @viewport { width: device-width; }
        body{
            font: 94.5% "Trebuchet MS", sans-serif;
            margin: 0px 0px 0px 0px;
        }
        html, body {
            padding: 0;
            margin: 0;
            height: 100%;
        }
        html[xmlns] #menu-bar {
            display: block;
        }
        .wrapper {
            margin: 0 auto;

            height: auto !important;
        }
        .clearfix:before,
        .clearfix:after {
            content: " ";
            display: table;
        }

        .clearfix:after {
            clear: both;
        }

        .clearfix {
            *zoom: 1;
        }
        .data_info{
            position: absolute;
            padding-left: 2px;
            top: 0px;
            right: 0px;
            width: 420px;
            height: 142px;
            background: #1f8833;
            color: #FFFFFF;
            border: dashed 2px;
            line-height: 2px;
            font: 100.5% "Verdana", sans-serif;
            background-image: url("/images/data_info_bg.jpg");
            background-repeat: no-repeat;

        }
        .plant-count{
            line-height: 1px;
        }
        .room-count{
            line-height: 1px;
        }

        .user-info{
            line-height: 1px;
        }

        .domain-info{
            line-height: 1px;
        }
        #icons {
            margin: 0;
            padding: 0;
        }
        #icons li {
            margin: 2px;
            position: relative;
            padding: 4px 0;
            cursor: pointer;
            float: left;
            list-style: none;
        }
        #icons span.ui-icon {
            float: left;
            margin: 0 4px;
        }
    </style>
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="/css/cg_header_nav_menu.css" type="text/css" />
    <link rel="stylesheet" href="/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/forms.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/jquery.dataTables.js"></script>
    <script type="text/javascript"> //<![CDATA[
        var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
        document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
        //]]>
    </script>
</head>
<body id="body">
<header class="wrapper clearfix">
    <article class="data_info">{$dateset}<br>
        <p class="plant-count">Total Plants: 3</p>
        <p class="room-count">Total Rooms/Farms: 1</p>
        <p class="user-info">Logged in user: {$user_loggedin}</p>
        <p class="domain-info">Domain prefix: {$domain_pref}.constantgrower.com</p>
    </article>
    <article><a href="/?page=dashboard"><img src="/images/cg_logo.png" width="420" height="143" border="0"></a></article>

    <nav>
        <div id="mbmcpebul_wrapper">
            <ul id="mbmcpebul_table" class="mbmcpebul_menulist css_menu">
                <li><div class="buttonbg gradient_button gradient36"><a href="/" class="button_1">Home</a></div>
                    <ul class="gradient_menu gradient130">
                        <li><a href="/?page=logout" title="">Logout</a></li>
                    </ul></li>
                </li>
                <li title="Plant Management"><div class="buttonbg gradient_button gradient36" style="width: 77px;"><div class="arrow"><a href="/plants/">Plants</a></div></div>
                    <ul class="gradient_menu gradient130">
                        <li><a href="/plants/?page=activelist" title="Active Plant List">Active List</a></li>
                        <li><a href="/plants/?page=seedlib" title="Breeder and Strain Libraries">Seed Libraries</a></li>
                        <li><a href="/plants/?page=feeding" title="Feeding Schedules">Feeding Schedules</a></li>
                        <li><a href="/plants/?page=reports" title="Plant Reports">Reports</a></li>
                    </ul></li>
                <li title="Grow Room Management"><div class="buttonbg gradient_button gradient36" style="width: 82px;"><div class="arrow"><a href="/rooms/">Rooms</a></div></div>
                    <ul class="gradient_menu gradient101">
                        <li><a href="/rooms/?page=activelist" title="Active Grow Rooms">Active Rooms</a></li>
                        <li><a href="/rooms/?page=assets" title="Grow Room Assets">Assets</a></li>
                        <li><a href="/rooms/?page=reports" title="Grow Room Reports">Reports</a></li>
                    </ul></li>
                <li title="Cloning Management"><div class="buttonbg gradient_button gradient36" style="width: 88px;"><div class="arrow"><a href="/cloning/">Cloning</a></div></div>
                    <ul class="gradient_menu gradient72">
                        <li><a href="/cloning/?page=activelist" title="Active List">Clone List</a></li>
                        <li><a href="/cloning/?page=reports" title="Cloning Reports">Reports</a></li>
                    </ul></li>
                <li title="Finanace Management"><div class="buttonbg gradient_button gradient36" style="width: 97px;"><div class="arrow"><a href="/finance/">Finances</a></div></div>
                    <ul class="gradient_menu gradient72">
                        <li><a href="/financing/?page=budgets" title="Budget Management">Budgeting</a></li>
                        <li><a href="/financing/?page=cost" title="Cost Calculations">Cost Calculations</a></li>
                    </ul></li>
                <li title="System Configuration"><div class="buttonbg gradient_button gradient36" style="width: 130px;"><div class="arrow"><a href="/configuration/">Configuration</a></div></div>
                    <ul class="gradient_menu gradient72">
                        <li><a href="/configuration/?page=system_management" title="">System Settings</a></li>
                        <li><a href="/configuration/?page=backups" title="">Backup</a></li>
                    </ul></li>
                <li title="About Constant Grower"><div class="buttonbg gradient_button gradient36" style="width: 74px;"><div class="arrow"><a href="/about/">About</a></div></div>
                    <ul class="gradient_menu gradient101">
                        <li><a href="/about/?page=support" title="Help and  Support">Support</a></li>
                        <li><a href="/about/?page=contact" title="Contact Us">Contact</a></li>
                        <li><a href="/about/?page=release_info" title="Release Information">Release Info</a></li>
                    </ul></li>
            </ul>
        </div>
    </nav>
</header>

