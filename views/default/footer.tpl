<script type="text/javascript" src="/js/cg_header_nav_menu.js"></script>

<div class="footer-container clearfix">
    <footer class="footer">
        <div style="margin: 0px 0px 0px 0px; left: -2px; position: fixed;"><img src="http://l.yimg.com/a/i/us/we/52/14.gif"/><b>Current Conditions:</b> Light Snow, 31 F</div>
        <div class="footer-group first" style="border-left: solid 0px #fff">
            <a href="/?page=default">Home</a>
        </div>
        <div class="footer-group">
            <p><a href="/about/?page=support">Help</a></p>
        </div>
        <div class="footer-group">
            <p>
                <a target="_blank" href="https://forums.constantfarmer.com/forumdisplay.php?11-Constant-Grower-App">Constant Grower Community</a>
            </p>
        </div>
        <div class="footer-group">
            <p><a href="/about/?page=tos">Terms of Use</a></p>
        </div>
        <div id="comodo_secure">
            <script language="JavaScript" type="text/javascript">
                TrustLogo("/images/comodo_secure_100x85_transp.png", "CL1", "none");
            </script>
        </div>
    </footer>
</div>
</body>
</html>