<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>Constant Grower</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="robots" content="index, follow">
    <link href="/css/jquery-ui.css" rel="stylesheet">

    <link rel="stylesheet" href="/css/constant_grower.css" type="text/css" media="all">
    <link rel="stylesheet" href="/css/jquery.mobile.icons.min.css" />
    <link rel="stylesheet" href="/css/mobile/login.css" type="text/css" media="all">
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.min.css" />
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>

    <style type="text/css">
    #custom_msg{
        background: #F9F9F9;
        border-color: #F9FF9;
    }
        .ui-widget{
            background-color: #F9F9F9 !important;
        }

    </style>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script>
        function custom_alert(output_msg, title_msg)
        {
            if (!title_msg)
                title_msg = 'Alert';

            if (!output_msg)
                output_msg = 'No Message to Display.';

            $("<div id=custom_msg></div>").html(output_msg).dialog({
                title: title_msg,
                resizable: false,
                width: 320,
                height: 220,
                modal: false,
                buttons: {
                    "Ok": function()
                    {
                        $( this ).dialog( "close" );
                    }
                }
            });
        }




        $(function() {
            var empty_msg = 'Empty values detected, please fill in all values';
            // url: '/authentication/?page=facebookauth',
            $("#AUTH_USER").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: '/authentication/?page=auth',
                    data: $(this).serialize(),
                    success: function(data)
                    {
                        if (data === "OK"){
                            window.location = '/?page=dashboard';
                            //custom_alert("Username and password OK","Notice");
                        } else if (data === "EMPTY") {
                            custom_alert(empty_msg, "Error Notice");
                        }else if( data === "AUTH_FAIL" ){
                            custom_alert("Bad username and password entered.<br>Please try again.", "Authentication Failed");
                        }
                    }
                });;

            });



        });
    </script>
</head>
<body id="body">


<!-- Start Login page -->
<div data-role="page" data-theme="a" id="Home" >
    <div data-role="header" class="header_bar" data-id="headernav" data-position="fixed" >
        <h1>Constant Grower Login</h1>
    </div>
<!-- login form -->
    <form id="AUTH_USER" method="POST" action="/authentication/?page=auth">
    <label for="user_name">Username:</label>
    <input type="text" name="user_name" id="user_name" value="">
    <label for="pass_word">Password:</label>
    <input type="password" name="pass_word" id="pass_word" value="">
    <label for="cg_domain_prefix">Domain Prefix:</label>
    <input type="text" id="cg_domain_prefix_input" name="cg_domain_prefix" id="cg_domain_prefix">
    <input type="submit"  value="Login" data-icon="user" data-theme="a">
    </form>
<!-- end login form -->

    <div class="navfooter footer_bar" data-theme="b " data-role="footer" data-id="footernav" data-position="fixed">

    </div><!-- /footer -->
</div>
<!-- End Home page -->

    </body>
</html>