<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>Constant Grower</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noarchive">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/css/jquery-ui.css" rel="stylesheet">

    <style type="text/css">
        body{
            margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px;
            background-color:#000000;
            background-image:url(images/cg_grower-bg.jpeg);
            background-size:cover;
        }

        input, text {
            font-family:Helvetica, Arial, sans-serif;
            font-size:14px;
            font-weight:100;
            color:#7F7F7F;
        }
        #username_input {
            position:relative;
            width:314px;
            height:38px;
            border:1px #979494;
        }

        #password_input{
            position:relative;
            width:314px;
            height:38px;
            border:1px #979494;
        }

        #cg_domain_prefix_input{
            position:relative;
            width:140px;
            height:38px;
            border:1px #979494;
        }

        #Table_01 {
            position:absolute;
            left:0px;
            top:0px;
            width:800px;
            height:458px;
        }

        #login-form-01 {
            position:absolute;
            left:0px;
            top:0px;
            width:800px;
            height:158px;
        }

        #login-form-02 {
            position:absolute;
            left:0px;
            top:158px;
            width:231px;
            height:300px;
        }

        #username {
            position:absolute;
            left:231px;
            top:158px;
            width:314px;
            height:38px;
        }

        #user {
            position:absolute;
            left:545px;
            top:158px;
            width:24px;
            height:38px;
        }

        #login-form-05 {
            position:absolute;
            left:569px;
            top:158px;
            width:231px;
            height:300px;
        }

        #login-form-06 {
            position:absolute;
            left:231px;
            top:196px;
            width:338px;
            height:42px;
        }

        #password {
            position:absolute;
            left:231px;
            top:238px;
            width:314px;
            height:38px;
        }

        #lock {
            position:absolute;
            left:545px;
            top:238px;
            width:24px;
            height:38px;
        }

        #login-form-09 {
            position:absolute;
            left:231px;
            top:276px;
            width:338px;
            height:34px;
        }

        #cg-domain-prefix {
            position:absolute;
            left:231px;
            top:310px;
            width:140px;
            height:38px;
        }

        #domain-prefix {
            position:absolute;
            left:371px;
            top:310px;
            width:174px;
            height:38px;
        }

        #domain {
            position:absolute;
            left:545px;
            top:310px;
            width:24px;
            height:57px;
        }

        #login-form-13 {
            position:absolute;
            left:231px;
            top:348px;
            width:314px;
            height:19px;
        }

        #logiin-submit-button {
            position:absolute;
            left:231px;
            top:367px;
            width:338px;
            height:40px;
            background-color: #F8F8F8;
        }

        #login-form-15 {
            position:absolute;
            left:231px;
            top:407px;
            width:338px;
            height:51px;
        }

        .login_submit input {
            cursor:pointer;
            width:338px;
            height:40px;
            border: none;
        }
        #forgot_pass-link {
            width: 120px;
            height: 30px;
            font-size: .8em;
            position: relative;
        }

        #new_register-link {
            width: 88px;
            height: 30px;
            font-size: .8em;
            position: relative;
        }
        #button_login {
            width: 88px;
            height: 30px;
            font-size: .8em;
            position: relative;
        }
        #comodo_secure{
            position: fixed;
            bottom: 0px;
            right: 6px;
        }


    </style>
->
    <script type="text/javascript"> //<![CDATA[
        var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
        document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
        //]]>
    </script>
</head>
<body>

<div id="Table_01">
    <form id="AUTH_USER" method="POST" action="/authentication/?page=auth">
        <div id="login-form-01">
            <img src="images/login_form_01.png" width="800" height="158" alt="" />
            agent: {$the_agent}
        </div>
        <div id="login-form-02">
            <img src="images/login_form_02.png" width="231" height="300" alt="" />
        </div>
        <div id="username">
            <input id="username_input" type="text" name="user_name" value="" width="314" height="38" alt="" />
        </div>
        <div id="user">
            <img src="images/user.png" width="24" height="38" alt="" />
        </div>
        <div id="login-form-05">
            <img src="images/login_form_05.png" width="231" height="300" alt="" />
        </div>
        <div id="login-form-06">
            <img src="images/login_form_06.png" width="338" height="42" alt="" />
        </div>
        <div id="password">
            <input type="password" id="password_input" name="pass_word" value="" width="314" height="38" alt="" />
        </div>
        <div id="lock">
            <img src="images/lock.png" width="24" height="38" alt="" />
        </div>
        <div id="login-form-09">
            <img src="images/login_form_09.png" width="338" height="34" alt="" />
        </div>
        <div id="cg-domain-prefix">
            <input id="cg_domain_prefix_input" name="cg_domain_prefix" value="" width="140" height="38" alt="" />
        </div>
        <div id="domain-prefix">
            <img src="images/domain_prefix.png" width="174" height="38" alt="" />
        </div>
        <div id="domain">
            <img src="images/domain.png" width="24" height="57" alt="" />
        </div>
        <div id="login-form-13">
            <img src="images/login_form_13.png" width="314" height="19" alt="" />
        </div>
        <div id="logiin-submit-button">
            <button type="submit" class="login_submit" id="button_login" name="Login" >Login</button>
            <button type="submit" id="new_register-link" name="Register_user" >Register</button>
            <button type="submit" id="forgot_pass-link" name="Reset_pass" >Reset Pass</button>
        </div>
        <div id="login-form-15">
            <img src="images/login_form_15.png" width="338" height="51" alt="" />
        </div>
    </form>

</div>

</body>
<!-- ui-dialog -->
<div id="new_register">
    {include file="register_user.tpl"}
</div>
<div id="forgot_pass">

       <form method="POST" id="reset_password">
           <label id="password_reset">
                Email: <input type="text" name="email" />
                <input type="submit" id="send_reset_email" name="send_reset_email" />
           </label>
       </form>

</div>
<div id="comodo_secure">
    <script language="JavaScript" type="text/javascript">
        TrustLogo("/images/comodo_secure_100x85_transp.png", "CL1", "none");
    </script>
</div>
<script src="/js/jquery.js"></script>
<script src="/js/jquery-ui.js"></script>
<script>
    function custom_alert(output_msg, title_msg)
    {
        if (!title_msg)
            title_msg = 'Alert';

        if (!output_msg)
            output_msg = 'No Message to Display.';

        $("<div></div>").html(output_msg).dialog({
            title: title_msg,
            resizable: false,
            width: 550,
            height: 220,
            modal: true,
            buttons: {
                "Ok": function()
                {
                    $( this ).dialog( "close" );
                }
            }
        });
    }

    $( "#button_login" ).button({
        icons: {
            primary: "ui-icon-check"
        },
        label: "Login" });

    $( "#button_register" ).button({
        icons: {
            primary: "ui-icon-check"
        },
        label: "Register" });

    $( "#new_register-link" ).button({
        icons: {
            primary: "ui-icon-person"
        },
        label: "Register" });

    $( "#forgot_pass-link" ).button({
        icons: {
            primary: "ui-icon-unlocked"
        },
        label: "Reset Pass" });

    var empty_msg = 'Empty values detected, please fill in all values';
    // url: '/authentication/?page=facebookauth',
    $("#AUTH_USER").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '/authentication/?page=auth',
            data: $(this).serialize(),
            success: function(data)
            {
                if (data === "OK"){
                    window.location = '/?page=dashboard';
                    //custom_alert("Username and password OK","Notice");
                } else if (data === "EMPTY") {
                    custom_alert(empty_msg, "Error Notice");
                }else if( data === "AUTH_FAIL" ){
                    custom_alert("Bad username and password entered.<br>Please try again.", "Authentication Failed");
                }
            }
        });;

    });

    $("#reset_password").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '/?page=reset_password',
            data: $(this).serialize(),
            success: function(data)
            {

                if (data === "SUCCESS"){
                    $("#forgot_pass").dialog("close");
                    custom_alert("Your password has now been reset, please check your email for which you signed up with.", "Password Reset Notice");
                } else if (data === "FAILED") {
                    $("#forgot_pass").dialog("close");
                    custom_alert("Internal Error, please come back and try later or contact support.", "Error Notice");
                }else if( data === "EMAIL_NOT_FOUND" ){
                    custom_alert("Email not found.<br>Please try again.", "Email Not Found");
                }
            }
        });

    });


    $("#REGISTER_USER").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '/?page=register_new_user',
            data: $(this).serialize(),
            success: function(data)
            {

                if (data === "SUCCESS"){
                    $("#new_register").dialog("close");
                    custom_alert("Your account is under review for new registration, please check your email for details.", "New User Registration");
                } else if (data === "EMPTY") {
                    custom_alert("Username, Password or Domain Prefix was left out.<br>Please enter in all field's Username, Password and Domain Prefix", "Error Notice");
                } else if (data === "FAILED") {
                    $("#new_register").dialog("close");
                    custom_alert("Internal Error, Unable to process any new registrations at the moment, please visit us <a target=\"_blank\" href=\"https://forums.constantfarmer.com/forumdisplay.php?11-Constant-Grower-App\">online</a> for additional information.", "Error Notice");
                }
            }
        });

    });


    /**
     * forgot pass dialog
     */
    $( "#forgot_pass" ).dialog({
        autoOpen: false,
        title: "Reset Your Password",
        width: 500,
        height: 150,
        modal: true
    });

    // Link to open forgot pass dialog
    $( "#forgot_pass-link" ).click(function( event ) {
        $( "#forgot_pass" ).dialog( "open" );
        event.preventDefault();
    });

    // Hover states on the static widgets for forgot pass dialog
    $( "#forgot_pass-link, #icons li" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
    );





    /**
     * register dialog
     */
    $( "#new_register" ).dialog({
        autoOpen: false,
        title: "New User Registration",
        width: 800,
        height: 600,
        modal: true,
        /*
        buttons: [
            {
                text: "Ok",
                click: function() {
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Cancel",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ]
        */
    });

    // Link to open new_register dialog
    $( "#new_register-link" ).click(function( event ) {
        $( "#new_register" ).dialog( "open" );
        event.preventDefault();
    });

    // Hover states on the static widgets for forgot pass dialog
    $( "#new_register-link, #icons li" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
    );
</script>

</body>
</html>