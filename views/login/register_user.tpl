<style type="text/css">
    input, text {
        font-family:Helvetica, Arial, sans-serif;
        font-size:14px;
        font-weight:100;
        color:#7F7F7F;
    }

    #register_Table_register {
        position:absolute;
        left:0px;
        top:0px;
        width:800px;
        height:458px;
    }

    #register_new-user-registration-form-01 {
        position:absolute;
        left:0px;
        top:0px;
        width:800px;
        height:123px;
    }

    #register_new-user-registration-form-02 {
        position:absolute;
        left:0px;
        top:123px;
        width:231px;
        height:335px;
    }

    #register_username {
        position:absolute;
        left:231px;
        top:123px;
        width:319px;
        height:38px;
    }

    #register_user-icon {
        position:absolute;
        left:550px;
        top:123px;
        width:19px;
        height:38px;
    }

    #register_new-user-registration-form-05 {
        position:absolute;
        left:569px;
        top:123px;
        width:231px;
        height:335px;
    }

    #register_new-user-registration-form-06 {
        position:absolute;
        left:231px;
        top:161px;
        width:338px;
        height:25px;
    }

    #register_password {
        position:absolute;
        left:231px;
        top:186px;
        width:319px;
        height:38px;
    }

    #register_lock-icon {
        position:absolute;
        left:550px;
        top:186px;
        width:19px;
        height:38px;
    }

    #register_new-user-registration-form-09 {
        position:absolute;
        left:231px;
        top:224px;
        width:338px;
        height:26px;
    }

    #register_domain-prefix {
        position:absolute;
        left:231px;
        top:250px;
        width:141px;
        height:38px;
    }

    #register_cg-domain {
        position:absolute;
        left:372px;
        top:250px;
        width:178px;
        height:38px;
    }

    #register_domain-icon {
        position:absolute;
        left:550px;
        top:250px;
        width:19px;
        height:38px;
    }

    #register_new-user-registration-form-13 {
        position:absolute;
        left:231px;
        top:288px;
        width:141px;
        height:26px;
    }

    #register_cgdomain {
        position:absolute;
        left:372px;
        top:288px;
        width:178px;
        height:26px;
    }

    #register_new-user-registration-form-15 {
        position:absolute;
        left:550px;
        top:288px;
        width:19px;
        height:26px;
    }

    #register_new_email-address {
        position:absolute;
        left:231px;
        top:314px;
        width:319px;
        height:38px;
    }

    #register_email-icon {
        position:absolute;
        left:550px;
        top:314px;
        width:19px;
        height:38px;
    }

    #register_new-user-registration-form-18 {
        position:absolute;
        left:231px;
        top:352px;
        width:338px;
        height:45px;
    }

    #register_submit {
        position:absolute;
        background-color: #f8f8f8;
        left:231px;
        top:397px;
        width:338px;
        height:40px;
    }

    #register_new-user-registration-form-20 {
        position:absolute;
        left:231px;
        top:437px;
        width:338px;
        height:21px;
    }

    #new_username_input {
        position:relative;
        width:319px;
        height:36px;
        border:1px #979494;
    }

    #new_password_input{
        position:relative;
        width:319px;
        height:36px;
        border:1px #979494;
    }

    #new_email_input{
        position:relative;
        width:319px;
        height:36px;
        border:1px #979494;
    }

    #cg_domain_prefix_input{
        position:relative;
        width:141px;
        height:36px;
        border:1px #979494;
    }




</style>

<div id="register_Table_register">
    <form method="POST" id="REGISTER_USER" >
        <div id="register_new-user-registration-form-01">
            <img src="new_user_registration_images/new_user_registration_form_01.png" width="800" height="123" alt="" />
        </div>
        <div id="register_new-user-registration-form-02">
            <img src="new_user_registration_images/new_user_registration_form_02.png" width="231" height="335" alt="" />
        </div>
        <div id="register_username">
            <input id="new_username_input" type="text" name="new_user_name" value="" width="319" height="38" alt="" />
        </div>
        <div id="register_user-icon">
            <img src="new_user_registration_images/user_icon.png" width="24" height="38" alt="" />
        </div>
        <div id="register_new-user-registration-form-05">
            <img src="new_user_registration_images/new_user_registration_form_05.png" width="231" height="335" alt="" />
        </div>
        <div id="register_new-user-registration-form-06">
            <img src="new_user_registration_images/new_user_registration_form_06.png" width="338" height="25" alt="" />
        </div>
        <div id="register_password">
            <input id="new_password_input" name="new_pass_word" value="" width="319" height="38" alt="" />
        </div>
        <div id="register_lock-icon">
            <img src="new_user_registration_images/lock_icon.png" width="19" height="38" alt="" />
        </div>
        <div id="register_new-user-registration-form-09">
            <img src="new_user_registration_images/new_user_registration_form_09.png" width="338" height="26" alt="" />
        </div>
        <div id="register_domain-prefix">
            <input id="cg_domain_prefix_input" name="new_cg_domain_prefix" value="" width="141" height="38" alt="" />
        </div>
        <div id="register_cg-domain">
            <img src="new_user_registration_images/cg_domain.png" width="178" height="38" alt="" />
        </div>
        <div id="register_domain-icon">
            <img src="new_user_registration_images/domain_icon.png" width="19" height="38" alt="" />
        </div>
        <div id="register_new-user-registration-form-13">
            <img src="new_user_registration_images/new_user_registration_form_13.png" width="141" height="26" alt="" />
        </div>
        <div id="register_cgdomain">
            <img src="new_user_registration_images/cgdomain.png" width="178" height="26" alt="" />
        </div>
        <div id="register_new-user-registration-form-15">
            <img src="new_user_registration_images/new_user_registration_form_15.png" width="19" height="26" alt="" />
        </div>
        <div id="register_new_email-address">
            <input id="new_email_input" type="text" name="new_email_address" value="" width="319" height="38" alt="" />
        </div>
        <div id="register_email-icon">
            <img src="new_user_registration_images/email_icon.png" width="19" height="38" alt="" />
        </div>
        <div id="register_new-user-registration-form-18">
            <img src="new_user_registration_images/new_user_registration_form_18.png" width="338" height="45" alt="" />
        </div>
        <div id="register_submit">
            <input type="submit" class="register_submit" id="button_register" name="register"  width="338" height="40" ></input>
        </div>
        <div id="register_new-user-registration-form-20">
            <img src="new_user_registration_images/new_user_registration_form_20.png" width="338" height="21" alt="" />
        </div>
    </form>
</div>
