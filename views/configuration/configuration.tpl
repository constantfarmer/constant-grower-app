<style type="text/css">
.tab_page{
    height: auto;
}
{foreach from=$users item=userObj}
#edit_user-link{$userObj.user_id} {
    font-size: 1em;
    position: relative;
}
#delete_user-link{$userObj.user_id} {
    font-size: 1em;
    position: relative;
}
{/foreach}
</style>
<link href="/css/dataTables.jqueryui.css" rel="stylesheet">
<script>
    function custom_alert(output_msg, title_msg)
    {
        if (!title_msg)
            title_msg = 'Alert';

        if (!output_msg)
            output_msg = 'No Message to Display.';

        $("<div></div>").html(output_msg).dialog({
            title: title_msg,
            resizable: false,
            width: 550,
            height: 220,
            modal: true,
            buttons: {
                "Ok": function()
                {
                    $( this ).dialog( "close" );
                    window.location = '/configuration/?page=system_management';
                }
            }
        });
    }

    $(function() {
        $( "#config_tabs" ).tabs();
        //DataTables
        $('#user_grid').DataTable({
            "pageLength": 5
        });


        $("#edit_system_settings").submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: '/configuration/?page=edit_system',
                data: $(this).serialize(),
                success: function(data)
                {

                    if (data === "SUCCESS"){
                        custom_alert("System settings edited.", "New Settings Completed");
                    }else if (data === "INCOMPLETE") {
                        custom_alert("Not all data was entered, please try again..", "Error Notice");
                    } else if (data === "FAILED") {
                        custom_alert("Internal Error, contact support.", "Error Notice");
                    }
                }
            });

        });

        {foreach from=$users item=userObj}
        $("#USER_EDIT").submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: '/configuration/?page=edit_user',
                data: $(this).serialize(),
                success: function(data)
                {

                    if (data === "SUCCESS"){
                        $("#edit_user{$userObj.user_id}").dialog("close");
                        custom_alert("Your details have been updated.", "User Edited");

                    } else if (data === "FAILED") {
                        $("#edit_user{$userObj.user_id}").dialog("close");
                        custom_alert("Internal Error, check debug log console if possible or contact support.", "Error Notice");
                    }
                }
            });

        });

        $("#USER_DELETE").submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: '/configuration/?page=delete_user',
                data: $(this).serialize(),
                success: function(data)
                {

                    if (data === "SUCCESS"){
                        $("#delete_user{$userObj.user_id}").dialog("close");
                        custom_alert("User account deleted.", "User Removed");
                    } else if (data === "FAILED") {
                        $("#delete_user{$userObj.user_id}").dialog("close");
                        custom_alert("Internal Error, contact support.", "Error Notice");
                    }
                }
            });

        });
        /**
         * edit user {$userObj.user_id}
         */
        $( "#edit_user{$userObj.user_id}" ).dialog({
            autoOpen: false,
            title: "Editing User {$userObj.user_name}",
            width: 600,
            height: 350,
            dialogClass: "pure-form pure-form-aligned pure-control-group",
            modal: true
        });

        // Link to open forgot pass dialog
        $( "#edit_user-link{$userObj.user_id}" ).click(function( event ) {
            $( "#edit_user{$userObj.user_id}" ).dialog( "open" );
            event.preventDefault();
        });

        // Hover states on the static widgets for forgot pass dialog
        $( "#edit_user-link{$userObj.user_id}, #icons li" ).hover(
                function() {
                    $( this ).addClass( "ui-state-hover" );
                },
                function() {
                    $( this ).removeClass( "ui-state-hover" );
                }
        );

        /**
         * delete user {$userObj.user_id}
         */
        $( "#delete_user{$userObj.user_id}" ).dialog({
            autoOpen: false,
            title: "Deleting User {$userObj.user_name}",
            width: 600,
            height: 250,
            modal: true
        });

        // Link to open forgot pass dialog
        $( "#delete_user-link{$userObj.user_id}" ).click(function( event ) {
            $( "#delete_user{$userObj.user_id}" ).dialog( "open" );
            event.preventDefault();
        });

        // Hover states on the static widgets for forgot pass dialog
        $( "#delete_user-link{$userObj.user_id}, #icons li" ).hover(
                function() {
                    $( this ).addClass( "ui-state-hover" );
                },
                function() {
                    $( this ).removeClass( "ui-state-hover" );
                }
        );
        {/foreach}

    });
</script>



<div id="config_tabs" class="tab_page">
    <ul>
        <li><a href="#system">System</a></li>
        <li><a href="#users">Users</a></li>
        <li><a href="#dispensary">Dispensary</a></li>
    </ul>
    <div id="system">
            <h3>Editing System Settings</h3>
            <form id="edit_system_settings" method="POST" action="/configuration/?page=edit_system" class="pure-form pure-form-aligned">
                <input type="hidden" name="cfg_system_id" value="{$system_Settings["sysid"]}">
                <fieldset>
                    <div class="pure-control-group">
                        <label for="garden_name">Garden Name</label>
                        <input id="garden_name" name="cfg_garden_name" value="{$system_Settings["garden"]}" type="text" placeholder="Garden Name">
                    </div>

                    <div class="pure-control-group">
                        <label for="state">State</label>
                        <input id="state" name="cfg_state" value="{$system_Settings["state"]}" type="text" placeholder="State">
                    </div>

                    <div class="pure-control-group">
                        <label for="city">City</label>
                        <input id="city" name="cfg_city" value="{$system_Settings["city"]}" type="text" placeholder="City">
                    </div>

                    <div class="pure-control-group">
                        <label for="zip">Zip Code</label>
                        <input id="zip" name="cfg_zip" value="{$system_Settings["zipcode"]}" type="text" placeholder="Zip Code">
                    </div>

                    <div class="pure-controls">
                        <button type="submit" class="pure-button pure-button-primary">Submit</button>
                    </div>

                </fieldset>
            </form>

    </div>
    <div id="users">
        <h3>User List</h3>
        <table id="user_grid"  cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Action</th>
                <th>User ID</th>
                <th>User</th>
                <th>User Level</th>
                <th>Domain Prefix</th>
                <th>Email</th>
                <th>Alias</th>
                <th>Last Login</th>
            </tr>
            </thead>
            <tbody>

            {foreach from=$users item=userObj}
                <tr>
                    <td id="first"><a id="edit_user-link{$userObj.user_id}" href="#">Edit</a> | <a id="delete_user-link{$userObj.user_id}" href="#">Delete</a></td>
                    <td>{$userObj.user_id}</td>
                    <td>{$userObj.user_name}</td>
                    <td>{$userObj.user_level}</td>
                    <td>{$userObj.domain_prefix}</td>
                    <td><a href="mailto:{$userObj.user_email}">{$userObj.user_email}</a></td>
                    <td>{$userObj.user_alias}</td>
                    <td>{$userObj.last_login}</td>
                </tr>

                <div id="edit_user{$userObj.user_id}">

                    <form id="USER_EDIT" method="POST">
                    <fieldset>
                        <input type="hidden" name="USERID" value="{$userObj.user_id}">
                        <div class="pure-control-group">
                            <label for="edit_user_name">User Name</label>
                            <input id="edit_user_name" name="edit_user_name" type="text" value="{$userObj.user_name}" placeholder="User Name">
                        </div>

                        <div class="pure-control-group">
                            <label for="edit_password">Password</label>
                            <input id="edit_password" name="edit_password" type="text"  placeholder="Password">
                        </div>

                        <div class="pure-control-group">
                            <label for="alias_name">Name</label>
                            <input id="alias_name" name="edit_alias_name" type="text" value="{$userObj.user_alias}" placeholder="Name">
                        </div>

                        <div class="pure-control-group">
                            <label for="edit_email">Email</label>
                            <input id="edit_email" name="edit_email" type="text" value="{$userObj.user_email}" placeholder="Email">
                        </div>

                        <div class="pure-controls">
                            <button type="submit" class="pure-button pure-button-primary">Submit</button>
                        </div>

                    </fieldset>
                    </form>
                </div>

                <div id="delete_user{$userObj.user_id}">
                    <form id="USER_DELETE" method="POST">
                    Remove user {$userObj.user_name} ?
                        <input type="hidden" name="ID" value="{$userObj.user_id}">
                        <button type="submit" class="pure-button pure-button-primary">Submit</button>
                    </form>
                </div>
            {/foreach}
            </tbody>

        </table>

    </div>

    <div id="dispensary">
        <p></p>
        <p></p>
    </div>
</div>
