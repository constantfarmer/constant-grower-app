<?php 
      /** 
       * The Wild West FrameWork 
       * @copyright 2015 
       * 
       * loginModel 
       * 
       * 
       * Class login 
       * Extends MasterDb 
       */ 
           
      class loginModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig; 
      
      
          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              } 
          } 
      
          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          } 
      
      
      }


