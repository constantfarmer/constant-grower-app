<?php
/**
 * The Wild West FrameWork
 * @copyright 2015
 *
 * Class IndexModel
 */
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class IndexModel extends MasterDb{
    use DBConfig;
    use GeneralConfig;

    /**
     * @var string
     */
    public $q = "";

    /**
     * @var string
     */
    public $error;

    /**
     * @var Logger
     */
    public $logobj;

    /**
     * @var string
     */
    public $email = "";


    /**
     * IndexModel constructor.
     * @param $dsn
     * @param string $user
     * @param string $passwd
     */
    public function __construct($dsn, $user = "", $passwd = ""){
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
        $this->logobj       	    = new Logger();
    }


    /**
     * @param $email
     * @return bool
     */
    public function lookup_password($email){
        $this->email = $email;
        $this->q = self::query_single("SELECT alias_name, CGUser, CGPass FROM cg_users WHERE email = '$this->email'");
        if($this->q){
            $user           = $this->q["CGUser"];
            $name           = $this->q["alias_name"];
            $newpass        = self::generatePassword(8);
            $md5newpass     = md5($newpass);

            $email_message  = "Hello $name<br>Your username is $user and new password is $newpass<br>";

            try {
                $qup = self::query("UPDATE cg_users SET CGPass='$md5newpass' WHERE email='$email'");

                $mail   = new Message;
                $mail->setFrom('Constant Grower Security Team <admin@constantgrower.com>')
                    ->addTo("$email")
                    ->setSubject('Constant Grower Password Retrieval')
                    ->setHTMLBody("$email_message");

                $mailer = new SendmailMailer;
                $mailer->send($mail);

                $this->logobj->logit("message sent");
                echo "SUCCESS";
                return (true);
            }catch(Exception $e){
                $this->logobj->logit("failed to send message, exception $e caught" .$e->getTrace());
                echo "FAILED";
                return(false);
            }
        }else{
            echo "EMAIL_NOT_FOUND";
            return(false);
        }
    }




    public function register_new_user($username,$password,$domain_prefix,$email){
        $md5pass = md5($password);
        $this->logobj->logit("REGISTERING new user $username, $password, $domain_prefix, $email");
        $usercheck      = self::check_existing_user($username);
        $domaincheck    = self::check_existing_domain($domain_prefix);

        if($usercheck === "USERNAME_OK" && $domaincheck === "DOMAIN_OK"){
            $qobj = self::prepare("INSERT INTO cg_users(CGUser, CGPass, CGLevel, CGDomain_prefix, email, signup_date)
              VALUES(:CGUser, :CGPass, :CGLevel, :CGDomain_prefix, :Email, NOW())");

            try {

                $qobj->execute(array(
                    "CGUser"                => $username,
                    "CGPass"                => $md5pass,
                    "CGLevel"               => 8,
                    "CGDomain_prefix"       => $domain_prefix,
                    "Email"                 =>  $email
                ));
                /**
                 * send new account sign up
                 */
                $welcome_message  = "Hello and Welcome to Constant Grower, below you will find your account information.<br>
                Login: https://$domain_prefix.constantgrower.com/login/ <br>
                Username: $username <br>
                Password: $password <br>
                Email: $email <br>
                Support and Help: https://forums.constantfarmer.com/forumdisplay.php?11-Constant-Grower-App<br><br>";

                $mail   = new Message;
                $mail->setFrom('Constant Grower New User <new-signups@constantgrower.com>')
                    ->addTo("$email")
                    ->setSubject('Welcome to The Constant Grower')
                    ->setHTMLBody("$welcome_message");
                $mailer = new SendmailMailer;
                $mailer->send($mail);


                $this->logobj->logit("SETUP NEW USER $username, $email $domain_prefix.constantgrower.com");
                return(TRUE);
            }catch(PDOException $pdoe){
                $msg = "Caught exception:".$pdoe->getMessage() ."\ntrace :". $pdoe->getTrace() ." ";
                $this->logobj->logit($msg);
                return(FALSE);
            }
        }else{
            $this->logobj->logit("Username and Domain prefix check failed, exiting.");
            return(FALSE);
        }

    }

    /**
     * @param $username
     * @return string
     */
    public function check_existing_user($username){
        $username_check = self::query_single("SELECT ID FROM cg_users WHERE CGUser = '$username'");
        $uID            = $username_check["ID"];
        /**
         * check username
         */
        if ($uID <= 0) {
            $this->logobj->logit("USERNAME  check ok");
            return ("USERNAME_OK");
        }else{
            $this->logobj->logit("USERNAME $username found, check not ok");
            return(FALSE);
        }
    }

    /**
     * @param $domain_prefix
     * @return string
     */
        public function check_existing_domain($domain_prefix){
            $domain_prefix_check    = self::query_single("SELECT ID FROM cg_users WHERE CGDomain_prefix = '$domain_prefix'");
            $idcheck = $domain_prefix_check["ID"];
            /**
             * check domain prefix
             */

            if($idcheck <= 0){
                $this->logobj->logit("DOMAIN prefix check ok");
                return("DOMAIN_OK");
            }else{
                $this->logobj->logit("DOMAIN prefix not check ok");
            }
        }


    /**
     * @param int $nbBytes
     * @return string
     * @throws Exception
     */
    public function getRandomBytes($nbBytes = 32){
        $bytes = openssl_random_pseudo_bytes($nbBytes, $strong);
        if (false !== $bytes && true === $strong) {
            return $bytes;
        }
        else {
            throw new \Exception("Unable to generate secure token from OpenSSL.");
        }
    }

    /**
     * @param $length
     * @return string
     * @throws Exception
     */
    public function generatePassword($length){
        return substr(preg_replace("/[^a-zA-Z0-9]/", "", base64_encode(self::getRandomBytes($length+1))),0,$length);
    }

    /**  EXAMPLES
     * @return array
     */
    public function show_dbs(){
        $dbs = parent::query_all("SHOW DATABASES");
        return($dbs);
    }

    /**  EXAMPLES
     * @return array
     */
    public function show_db_vars(){
        $dbvars = parent::query_obj("SHOW VARIABLES");
        return($dbvars);
    }
}
