<?php 
      /** 
       * The Wild West FrameWork 
       * @copyright 2015 
       * 
       * configurationModel 
       * 
       * 
       * Class configuration 
       * Extends MasterDb 
       */ 
           
      class configurationModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig;

          /**
           * @var Logger
           */
          private $logobj;

          /**
           * @var array
           */
          public $user_arr = array();

          /**
           * @var array
           */
          public $data_arr = array();

          /**
           * @var string
           */
          public $sql = "";

          /**
           * @var
           */
          public $stmt;

          /**
           * configurationModel constructor.
           * @param $dsn
           * @param string $user
           * @param string $passwd
           */
          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              }

              $this->logobj               = new Logger();
          }

          /**
           * @return bool
           */
          public function get_system_settings(){
              $this->sql = self::query_single("SELECT id,garden_name,state,city,zip_code FROM cg_system_settings");
              $this->data_arr = array(
                  "sysid"     => $this->sql["id"],
                  "garden"    => $this->sql["garden_name"],
                  "state"     => $this->sql["state"],
                  "city"      => $this->sql["city"],
                  "zipcode"   => $this->sql["zip_code"]
              );

              return($this->data_arr);
          }

          /**
           * @return bool
           */
          public function edit_system_settings($sysid,$gardenname,$state,$city,$zip){
            try{
              $this->stmt  = self::prepare("UPDATE `cg_system_settings`
                                            SET `garden_name`      = :gardenname,
                                                      `state`      = :state,
                                                      `city`       = :city,
                                                      `zip_code`   = :zipcode
                                                      WHERE  `id`  = :ID
                                                      ");

              $this->stmt->execute(array(
                  "gardenname"  => $gardenname,
                  "state"       => $state,
                  "city"        => $city,
                  "zipcode"     => $zip,
                  "ID"          => $sysid
              ));
              return(TRUE);
          } catch(PDOException $e) {
            $msg = 'Caught exception: '.$e->getMessage().'';
            $this->logobj->logit($msg);
            return(FALSE);
      }
          }



          /**
           * @param $domain_prefix
           * @return array
           */
          public function get_all_users($domain_prefix){
              $sqlQuery = self::query_obj("SELECT * FROM cg_users WHERE CGDomain_prefix = '$domain_prefix'");
              foreach($sqlQuery as $sqlQ) {
                  $this->user_arr[] = array(
                      "user_id"         => $sqlQ->ID,
                      "user_name"       => $sqlQ->CGUser,
                      "user_pass"       => $sqlQ->CGPass,
                      "user_level"      => $sqlQ->CGLevel,
                      "domain_prefix"   => $sqlQ->CGDomain_prefix,
                      "user_email"      => $sqlQ->email,
                      "user_alias"      => $sqlQ->alias_name,
                      "signup_date"     => $sqlQ->signup_date,
                      "last_login"      => $sqlQ->last_login
                  );
              }
              return($this->user_arr);
          }


          /**
           * @param $userID
           * @param $new_username
           * @param $new_password
           * @param $new_name
           * @param $new_email
           * @return bool|string
           */
          public function edit_the_user($userID, $new_username, $new_password, $new_name, $new_email){
            try{
              $sql = self::prepare("UPDATE `cg_users`
                                    SET `CGUser`     = :username,
                                        `CGPass`     = :password,
                                        `alias_name` = :aliasname,
                                        `email`      = :email
                                        WHERE `ID` = :user_id");
                $sql->execute(array(
                    "username"      => $new_username,
                    "password"      => $new_password,
                    "aliasname"     => $new_name,
                    "email"         => $new_email,
                    "user_id"       => $userID
                ));
                return(TRUE);
            }
            catch(PDOException $e) {
                $msg = 'Caught exception: '.$e->getMessage().'';
                $this->logobj->logit($msg);
                return(FALSE);
            }
          }

          public function delete_the_user($theid){
              return(TRUE);
          }
          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          } 
      
      
      }


