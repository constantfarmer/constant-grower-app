<?php 
      /** 
       * The Wild West FrameWork 
       * @copyright 2015 
       * 
       * seedfinderModel 
       * 
       * 
       * Class seedfinder 
       * Extends MasterDb 
       */ 
           
      class seedfinderModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig; 
          use SeedFinderAPI;


          /**
           * @var int
           */
          public $breeder_id = 0;

          /**
           * @var string
           */
          public $breeder_name          = "";

          /**
           * @var string
           */
          public $breeder_display_name  = "";

          /**
           * @var string
           */
          public $breeder_logourl       = "";

          /**
           * @var int
           */
          public $strain_id = 0;

          /**
           * @var string
           */
          public $strain_name = "";

          /**
           * @var string
           */
          public $strain_display_name = "";

          /**
           * @var
           */
          public $stmt;

          /**
           * @var array
           */
          public $stmt_arr = array();

          /**
           * seedfinderModel constructor.
           * @param $dsn
           * @param string $user
           * @param string $passwd
           */
          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              } 
          } 
      
          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          }

          /**
           * @return \Psr\Http\Message\StreamInterface
           */
          public function get_all(){
              $client = new GuzzleHttp\Client(['verify' => false]);
              $request = $client->get("http://en.seedfinder.eu/api/json/ids.json?output=0&strains=1&ac=".self::sf_api_key());
              return($request->getBody());
          }

          /**
           * @return mixed
           */
          public function breeder_export(){
              $breeder_list = self::get_all();
              return(json_decode($breeder_list,true));
          }

          /**
           * @param $breeder_name
           * @param $breeder_display_name
           * @param $breeder_logourl
           * @return bool|string
           */
          public function import_breeder($breeder_name, $breeder_display_name, $breeder_logourl, $datestamp){
              $this->stmt = self::prepare("INSERT INTO cg_breeders(name, display_name, logo_url, date_added)
              VALUES(:name, :display_name, :logo_url, NOW())");

              try {
                  $this->stmt->execute(array(
                      "name"            => $breeder_name,
                      "display_name"    => $breeder_display_name,
                      "logo_url"        => $breeder_logourl
                  ));
                  echo "Inserted $breeder_name, $breeder_display_name, $breeder_logourl to db<br>";
                  return(true);
              }catch(PDOException $pdoe){
                  return("Caught exception:".$pdoe->getMessage() . $pdoe->getTrace());
              }

          }

          /**
           * @return bool|string
           */
          public function truncate_breeders(){
              try {
                  $this->stmt = self::query_single("TRUNCATE TABLE cg_breeders");
                    return(true);
              }catch(PDOException $e){
                  return("Caught exception:".$e->getMessage() . $e->getTrace());
              }
      }

          /**
           * @param $breeder_name
           * @return mixed
           */
          public function get_breeder_info_by_name($breeder_name){
              $this->stmt = self::query_single("SELECT id,display_name,logo_url,date_added FROM cg_breeders WHERE name = '$breeder_name'");
              $this->stmt_arr = array(
                  "breeder_id"           => $this->stmt["id"],
                  "breeder_display_name" => $this->stmt["display_name"],
                  "breeder_logo_url"     => $this->stmt["logo_url"],
                  "date_added"           => $this->stmt["date_added"]
              );
              return($this->stmt_arr);

          }

          /**
           * @param $breeder_id
           * @param $strain_name
           * @param $strain_display_name
           * @return bool|string
           */
          public function import_strains($breeder_id,$strain_name, $strain_display_name){
              $this->stmt = self::prepare("INSERT INTO cg_strain(name, display_name, breeder_id, date_added)
              VALUES(:name, :display_name, :breeder_id, NOW())");

              try {
                  $this->stmt->execute(array(
                      "name"            => $strain_name,
                      "display_name"    => $strain_display_name,
                      "breeder_id"      => $breeder_id
                  ));
                  echo "Inserted $strain_name, $strain_display_name, breeder id: $breeder_id to db<br>";

              }catch(PDOException $pdoe){
                  return("Caught exception:".$pdoe->getMessage() . $pdoe->getTrace());
              }

          }


      }


