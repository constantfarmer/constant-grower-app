<?php
      /** 
       * The Wild West FrameWork 
       * @copyright 2015 
       * 
       * yahoo_weatherModel 
       * 
       * 
       * Class yahoo_weather 
       * Extends MasterDb 
       */ 
           
      class yahoo_weatherModel  extends MasterDb{
          /**
           * use trait DBConfig
           */
          use DBConfig;

          /**
           * use trait GeneralConfig
           */
          use GeneralConfig;

          /**
           * use trait YahooWeatherAPI
           */
          use YahooWeatherAPI;

          /**
           * @var Logger
           */
          private $logobj;

          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              }
              $this->logobj    = new Logger();
          } 
      
          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          }

          /**
           * @param $yql_query
           * @return string
           */
          public function connect_to_yahoo($yql_query){
              $yql_query_url = self::yahoo_base_url() . "?q=" . urlencode($yql_query) . "&format=json";
              return ( $yql_query_url );
          }

          /**
           * @param $city
           * @param $state
           * @param $zipcode
           * @return mixed
           * @throws Exception
           */
          public function get_weather($zipcode){
              //$yq          =  "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='$city, $state')";
              $yq          = "select * from weather.forecast where (location = $zipcode)";
              $connection  = self::connect_to_yahoo($yq);
              $this->logobj->logit($yq);
              $session = curl_init($connection);
              curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
              $json = curl_exec($session);
              // Convert JSON to PHP associative array
              $phpObj =  json_decode($json,true);
              $this->logobj->logit($json);

              return($phpObj);
          }


      
      }


