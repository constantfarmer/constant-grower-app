<?php
      /** 
       * The Wild West FrameWork 
       * @copyright 2015 
       * 
       * authenticationModel 
       * 
       * 
       * Class authentication 
       * Extends MasterDb 
       */ 
           
      class authenticationModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig; 
      
      
          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              } 
          }

          /**
           * @param $username
           * @param $password
           * @return bool
           */
          public function db_auth_user($username,$password,$domain_prefix){
              $auth = self::query_all("SELECT ID FROM cg_users WHERE CGUser = '$username' AND CGPass = '$password' AND CGDomain_prefix = '$domain_prefix'");
              if($auth) {
                  self::update_logintime($username);
                  return (TRUE);
              }
          }

          /*
           *
           */
          public function update_logintime($username){
              self::query("UPDATE cg_users SET last_login = NOW() WHERE CGUser = '$username'");
          }
      
      
      }


