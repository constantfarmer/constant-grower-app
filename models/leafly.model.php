<?php 
      /** 
       * The Wild West FrameWork 
       * @copyright 2015 
       * 
       * leaflyModel 
       * 
       * 
       * Class leafly 
       * Extends MasterDb 
       */ 
           
      class leaflyModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig; 
      
      
          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              } 
          } 
      
          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          } 

    public function get_all_strains(){
              $client = new GuzzleHttp\Client(['verify' => false]);
              $request = $client->get("http://en.seedfinder.eu/api/json/ids.json?output=1&strains=1&ac=9df80cff63b1644546c899124e33d860");
              return($request->getBody());
          }      
      
      }


